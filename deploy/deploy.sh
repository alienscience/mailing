#!/bin/sh
set -e

mkdir -p tmp-deploy
cp deploy/install tmp-deploy/
cp deploy/mailing.toml tmp-deploy/
cp deploy/mailing.service tmp-deploy/
cp deploy/nginx.conf tmp-deploy/
cp mailing tmp-deploy/mailing
cd tmp-deploy
tar --zstd -cf mailing.tar.zst \
     install mailing mailing.service nginx.conf mailing.toml

# Use a checksum file to signal that the deploy is complete.
sha256sum "mailing.tar.zst" > "mailing.tar.zst.sha256sum"

sftp -P "${SSH_DEPLOY_PORT}" -o "StrictHostKeyChecking=no" \
     "${SSH_DEPLOY_USER}"@mail.alienscience.org:inbox/ << EOF
put -f *.tar.zst
put -f *.sha256sum
ls -lh
EOF

package main

import (
	"io"
	"log"
	"net"
	"strings"

	"gitlab.com/alienscience/mailing/internal/store"
	"gitlab.com/alienscience/mailing/pkg/mxdns"
	"gitlab.com/alienscience/mailing/pkg/smtp"
)

type smtpHandler struct {
	domains   []string
	mxDNS     *mxdns.MxDNS
	mailstore *store.MailStore
	traceDir  string
}

type sessionHandler struct {
	smtpHandler
	trace  tracer
	stored []store.MessageStore
}

func newSMTPHandler(domains []string, blocklists Blocklists, maildir string, trace string) (smtpHandler, error) {
	mailstore, err := store.NewMailstore(maildir)
	if err != nil {
		return smtpHandler{}, err
	}
	ipBlocklists := blocklists.DNS.IP
	domainBlocklists := blocklists.DNS.Domain
	return smtpHandler{
		domains:   domains,
		mxDNS:     mxdns.NewMxDNS(ipBlocklists, domainBlocklists),
		mailstore: mailstore,
		traceDir:  trace,
	}, nil
}

func (s smtpHandler) Session() (smtp.SessionHandler, smtp.SessionTracer) {
	trc := newTracer(s.traceDir)
	hdl := &sessionHandler{smtpHandler: s, trace: trc}
	return hdl, trc
}

func (s *sessionHandler) Hello(ip net.IP, domain string) *smtp.Response {
	s.trace.hello(domain)
	if strings.HasPrefix(domain, "[") {
		// Reject EHLO that uses IP addresses
		return smtp.BadHello
	}
	fcrdns, helloOk := s.mxDNS.CheckReverse(ip, domain)
	if !helloOk {
		return smtp.BadHello
	}
	if !fcrdns {
		return smtp.BadFcrdns
	}
	if s.mxDNS.IsIPBlocked(ip) {
		return smtp.IPOnBlocklist
	}
	if s.mxDNS.IsDomainBlocked(domain) {
		return smtp.DomainOnBlocklist
	}
	return smtp.OkReply
}

func (s *sessionHandler) MailFrom(ip net.IP, domain, reversePath string) *smtp.Response {
	return smtp.OkReply
}

func (s *sessionHandler) RcptTo(domain, reversePath, recipient string) *smtp.Response {
	mbox, err := smtp.ParseMailbox(recipient)
	if err != nil {
		return smtp.SyntaxError
	}
	// In the future, this will check mailboxes in the mailstore
	for _, d := range s.domains {
		if mbox.Domain == d {
			return smtp.OkReply
		}
	}
	return smtp.NoSuchUser
}

func (s *sessionHandler) Data(reversePath string, recipients []string) (io.WriteCloser, *smtp.Response) {
	store := s.mailstore.MessageStore(recipients)
	writer, err := store.Writer()
	if err != nil {
		log.Println("cannot create mailstore writer:", err)
		return nil, smtp.LocalError
	}
	s.stored = append(s.stored, store)
	return writer, smtp.StartMailInput
}

func (s *sessionHandler) End() {
	buf := s.trace.trace()
	if buf == nil {
		return
	}
	// Save an SMTP trace with each message
	for _, st := range s.stored {
		err := st.AttachTrace(buf)
		if err != nil {
			log.Println("cannot attach trace:", err)
			break
		}
	}
	// Save the SMTP trace standalone
	s.trace.save()
}

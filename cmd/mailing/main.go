package main

import (
	"flag"
	"log"

	"gitlab.com/alienscience/mailing/pkg/smtp"
)

var (
	configFile = flag.String("config", "mailing.toml", "configuration file")
)

func main() {
	log.SetFlags(0)
	flag.Parse()
	config := readConfig(*configFile)
	handler, err := newSMTPHandler(
		config.Domains,
		config.Blocklists,
		config.Maildir,
		config.Trace,
	)
	if err != nil {
		log.Fatal(err)
	}
	server, err := smtp.NewServer(
		smtp.WithFqdn(config.FQDN),
		smtp.WithHandler(handler),
		smtp.WithAddress(config.SMTP),
		smtp.WithTLS(config.Cert, config.Key),
	)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("%s SMTP server starting on %s", config.FQDN, config.SMTP)
	server.ListenAndServe()
}

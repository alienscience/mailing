package main

import (
	"log"
	"os"

	"github.com/kelseyhightower/envconfig"
	"github.com/pelletier/go-toml"
)

// Config holds the program configuration
type Config struct {
	FQDN       string
	SMTP       string
	Domains    []string
	Cert       string
	Key        string
	Maildir    string
	Trace      string
	Blocklists Blocklists
}

// Blocklists holds the configuration of blocklists
type Blocklists struct {
	DNS struct {
		IP     []string
		Domain []string
	}
}

func defaultConfig() Config {
	// In the future this will set values that are not in
	// the default configuration file.
	return Config{}
}

func readConfig(filename string) Config {
	config := defaultConfig()

	// Read in TOML config
	// nolint: gosec // config file can be anywhere
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	decoder := toml.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		log.Fatalf("cannot read %s: %s", filename, err)
	}
	// Override with environment variables
	err = envconfig.Process("mailing", &config)
	if err != nil {
		log.Fatalf("cannot override config: %s", err)
	}
	return config
}

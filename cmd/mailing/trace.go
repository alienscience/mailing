package main

import (
	"bytes"
	"fmt"
	"log"
	"os"

	"gitlab.com/alienscience/mailing/pkg/smtp"
)

type tracer interface {
	smtp.SessionTracer
	hello(string)
	trace() []byte
	save()
}

type trace struct {
	dir          string
	name         string
	serverBuffer [][]byte
	buf          bytes.Buffer
}

type emptyTracer struct {
	smtp.SessionTracer
}

func (t emptyTracer) hello(name string) {}
func (t emptyTracer) trace() []byte     { return nil }
func (t emptyTracer) save()             {}

func newTracer(dir string) tracer {
	if dir == "" {
		return emptyTracer{smtp.EmptyTracer}
	}
	return &trace{
		dir:          dir,
		serverBuffer: make([][]byte, 0, 4),
	}
}

func (t *trace) hello(name string) {
	t.name = name
}

func (t *trace) save() {
	name := "syntax_error"
	if t.name != "" {
		name = t.name
	}
	if err := os.MkdirAll(t.dir, 0700); err != nil {
		log.Printf("cannot create %s : %s", t.dir, err)
		return
	}
	filename := fmt.Sprintf("%s/%s.log", t.dir, name)
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600) // #nosec
	if err != nil {
		log.Printf("cannot open %s : %s", filename, err)
		return
	}
	_, err = file.Write(t.buf.Bytes())
	if err != nil {
		log.Printf("cannot write to %s : %s", filename, err)
		return
	}
}

func (t *trace) trace() []byte {
	return t.buf.Bytes()
}

func (t *trace) Client(b []byte) {
	_, _ = t.buf.Write([]byte("C: "))
	_, _ = t.buf.Write(b)
}

func (t *trace) Server(b []byte) {
	lines := bytes.SplitAfter(b, []byte("\r\n"))
	for _, line := range lines {
		if len(line) > 0 {
			t.serverBuffer = append(t.serverBuffer, line)
		}
	}
}

func (t *trace) ServerFlush() {
	for _, b := range t.serverBuffer {
		_, _ = t.buf.Write([]byte("S: "))
		_, _ = t.buf.Write(b)
	}
	t.serverBuffer = make([][]byte, 0, 4)
}

func (t *trace) Close() {
	_, _ = t.buf.Write([]byte("\r\n\r\n"))
}

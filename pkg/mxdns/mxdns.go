package mxdns

import (
	"net"
)

var localHost = net.IPv4(127, 0, 0, 1)

// MxDNS provides utilities for checking DNS blocklists.
type MxDNS struct {
	ipLists     []blocklist
	domainLists []blocklist
}

// NewMxDNS creates an MxDNS instance that will check the given blocklists.
func NewMxDNS(ipBlocklists []string, domainBlocklists []string) *MxDNS {
	mxdns := &MxDNS{
		ipLists:     make([]blocklist, len(ipBlocklists)),
		domainLists: make([]blocklist, len(domainBlocklists)),
	}
	for i, b := range ipBlocklists {
		mxdns.ipLists[i] = blocklist{domain: b}
	}
	for i, b := range domainBlocklists {
		mxdns.domainLists[i] = blocklist{domain: b}
	}
	return mxdns
}

// CheckDomain checks if the given domain is on the configured blocklists
func (m *MxDNS) CheckDomain(domain string) []Blocked {
	numBL := len(m.domainLists)
	ch := make(chan Blocked, numBL)
	for i := range m.domainLists {
		blist := &(m.domainLists[i])
		go blist.checkDomain(ch, domain)
	}
	ret := make([]Blocked, numBL)
	for i := 0; i < numBL; i++ {
		ret[i] = <-ch
	}
	return ret
}

// IsDomainBlocked checks if the given domain is on any of the configured blocklists.
func (m *MxDNS) IsDomainBlocked(domain string) bool {
	res := m.CheckDomain(domain)
	for _, r := range res {
		if r.Err == nil && r.IsBlocked {
			return true
		}
	}
	return false
}

// CheckIP checks if the given IP address is on the configured blocklists
// and returns a result for each blocklist.
func (m *MxDNS) CheckIP(ip net.IP) []Blocked {
	numBL := len(m.ipLists)
	ch := make(chan Blocked, numBL)
	for i := range m.ipLists {
		blist := &(m.ipLists[i])
		go blist.checkIP(ch, ip)
	}
	ret := make([]Blocked, numBL)
	for i := 0; i < numBL; i++ {
		ret[i] = <-ch
	}
	return ret
}

// IsIPBlocked checks if the given IP address is on any of the configured blocklists.
func (m *MxDNS) IsIPBlocked(ip net.IP) bool {
	if ip.Equal(localHost) {
		return false
	}
	res := m.CheckIP(ip)
	for _, r := range res {
		if r.Err == nil && r.IsBlocked {
			return true
		}
	}
	return false
}

// CheckReverse does both a reverse lookup and a FCRDNS check.
// The reverse lookup should match expectHost.
// The reverse lookup should resolve back to the given ip.
func (m *MxDNS) CheckReverse(ip net.IP, expectHost string) (fcrdns bool, expectedHost bool) {
	fcrdns, expectedHost = false, false
	if ip.Equal(localHost) {
		fcrdns, expectedHost = true, true
		return
	}
	names, err := net.LookupAddr(ip.String())
	if err != nil {
		return
	}
	expect := expectHost + "."
	for _, name := range names {
		if name == expect {
			expectedHost = true
			break
		}
	}
	for _, name := range names {
		addrs, err := net.LookupIP(name)
		if err != nil {
			continue
		}
		for _, addr := range addrs {
			if addr.Equal(ip) {
				fcrdns = true
				break
			}
		}
	}
	return
}

// Package mxdns provides DNS utilities for SMTP servers.
package mxdns

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	// The net package does not return the TTL for DNS entries.
	// Use this default for all DNS entries
	estimateTTL = 10 * time.Minute
	// Some blocklists are reached through the local default DNS server.
	// Occasionally recheck the DNS status of these blocklists.
	defaultDNSTTL = 24 * time.Hour
)

// Blocked holds the result of checking a single blocklist.
type Blocked struct {
	Blocklist string
	IsBlocked bool
	Err       error
}

type blocklist struct {
	domain     string
	resolver   *net.Resolver
	validUntil time.Time
	mux        sync.Mutex
}

func (b *blocklist) checkDomain(ch chan<- Blocked, domain string) {
	resolver, err := b.resolve()
	if err != nil {
		ch <- Blocked{Blocklist: b.domain, Err: err}
	}
	var sb strings.Builder
	_, _ = sb.WriteString(domain)
	_, _ = sb.WriteString(".")
	_, _ = sb.WriteString(b.domain)
	lookup := sb.String()
	ipaddrs, err := resolver.LookupHost(context.Background(), lookup)

	// An empty result is reported as an error but there is no constant to check
	// against. Consider all errors to mean that the host is not blocked.
	if err != nil {
		ch <- Blocked{Blocklist: b.domain, IsBlocked: false}
		return
	}
	if len(ipaddrs) > 0 {
		ch <- Blocked{Blocklist: b.domain, IsBlocked: true}
		return
	}
	ch <- Blocked{Blocklist: b.domain, IsBlocked: false}
}

func (b *blocklist) checkIP(ch chan<- Blocked, ip net.IP) {
	ipv4 := ip.To4()
	if ipv4 == nil {
		msg := fmt.Sprintf("Cannot convert %s to ipv4", ip)
		ch <- Blocked{Blocklist: b.domain, Err: errors.New(msg)}
		return
	}
	resolver, err := b.resolve()
	if err != nil {
		ch <- Blocked{Blocklist: b.domain, Err: err}
	}
	lookup, err := b.ipLookup(ipv4)
	if err != nil {
		ch <- Blocked{Blocklist: b.domain, Err: err}
		return
	}
	ipaddrs, err := resolver.LookupHost(context.Background(), lookup)
	// An empty result is reported as an error but there is no constant to check
	// against. Consider all errors to mean that the host is not blocked.
	if err != nil {
		ch <- Blocked{Blocklist: b.domain, IsBlocked: false}
		return
	}
	if len(ipaddrs) > 0 {
		ch <- Blocked{Blocklist: b.domain, IsBlocked: true}
		return
	}
	ch <- Blocked{Blocklist: b.domain, IsBlocked: false}
}

// Get the DNS server associated with the blocklist
func (b *blocklist) resolve() (*net.Resolver, error) {
	b.mux.Lock()
	defer b.mux.Unlock()

	if time.Now().After(b.validUntil) {
		if err := b.updateResolver(); err != nil {
			return nil, err
		}
	}
	return b.resolver, nil
}

func (b *blocklist) updateResolver() error {
	nameservers, _ := net.LookupNS(b.domain)
	// By default, use the local nameserver
	resolver := net.DefaultResolver
	validUntil := time.Now().Add(defaultDNSTTL)
	if len(nameservers) > 0 {
		// Use a random nameserver
		// #nosec
		nameserver := nameservers[rand.Intn(len(nameservers))]
		ips, err := net.LookupIP(nameserver.Host)
		if err != nil {
			return err
		}
		if len(ips) > 0 {
			resolver = &net.Resolver{
				PreferGo: true,
				Dial:     withNameserver(ips[0].String()),
			}
			validUntil = time.Now().Add(estimateTTL)
		}
	}
	b.resolver = resolver
	b.validUntil = validUntil
	return nil
}

// Convert an ipv4 address into a blocklist lookup
func (b *blocklist) ipLookup(ipv4 net.IP) (string, error) {
	var sb strings.Builder
	// Write ip address in reverse order
	for i := 3; i >= 0; i-- {
		triple := int(ipv4[i])
		_, err := sb.WriteString(strconv.Itoa(triple))
		if err != nil {
			return "", err
		}
		err = sb.WriteByte('.')
		if err != nil {
			return "", err
		}
	}
	_, _ = sb.WriteString(b.domain)
	return sb.String(), nil
}

// Returns a net.Resolver.Dial function that uses the given nameserver
func withNameserver(nameserver string) func(context.Context, string, string) (net.Conn, error) {
	override := nameserver + ":53"
	return func(ctx context.Context, network, address string) (net.Conn, error) {
		return net.Dial(network, override)
	}
}

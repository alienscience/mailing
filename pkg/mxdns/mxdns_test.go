package mxdns

import (
	"net"
	"testing"
)

var (
	ipBlocklists     = []string{"zen.spamhaus.org", "dnsbl-1.uceprotect.net"}
	domainBlocklists = []string{"dbl.spamhaus.org"}
	blockedIP        = net.IPv4(127, 0, 0, 2)
	blockedDomain    = "maxbounty.info"
	nonBlockedDomain = "alienscience.org"
	nonBlockedFqdn   = "mail.alienscience.org"
)

func TestIpBlocked(t *testing.T) {
	if testing.Short() {
		t.Skip("DNS Blocklist test")
	}
	m := NewMxDNS(ipBlocklists, domainBlocklists)
	res := m.CheckIP(blockedIP)
	for _, r := range res {
		if r.Err != nil {
			t.Error(r.Blocklist, r.Err)
		}
		if !r.IsBlocked {
			t.Errorf("%s does not block %s", r.Blocklist, blockedIP)
		}
	}
}

func TestIpNotBlocked(t *testing.T) {
	if testing.Short() {
		t.Skip("DNS Blocklist test")
	}
	m := NewMxDNS(ipBlocklists, domainBlocklists)
	testIps, err := net.LookupIP(nonBlockedFqdn)
	if err != nil {
		t.Error(err)
	}
	if len(testIps) == 0 {
		t.Errorf("No IP for %s", nonBlockedFqdn)
	}
	testIP := testIps[0]
	res := m.CheckIP(testIP)
	for _, r := range res {
		if r.Err != nil {
			t.Error(r.Blocklist, r.Err)
		}
		if r.IsBlocked {
			t.Errorf("%s blocks %s", r.Blocklist, testIP)
		}
	}
}

func TestDomainBlocked(t *testing.T) {
	if testing.Short() {
		t.Skip("DNS Blocklist test")
	}
	m := NewMxDNS(ipBlocklists, domainBlocklists)
	res := m.CheckDomain(blockedDomain)
	for _, r := range res {
		if r.Err != nil {
			t.Error(r.Blocklist, r.Err)
		}
		if !r.IsBlocked {
			t.Errorf("%s does not block %s", r.Blocklist, blockedDomain)
		}
	}
}

func TestDomainNotBlocked(t *testing.T) {
	if testing.Short() {
		t.Skip("DNS Blocklist test")
	}
	m := NewMxDNS(ipBlocklists, domainBlocklists)
	res := m.CheckDomain(nonBlockedDomain)
	for _, r := range res {
		if r.Err != nil {
			t.Error(r.Blocklist, r.Err)
		}
		if r.IsBlocked {
			t.Errorf("%s blocks %s", r.Blocklist, nonBlockedDomain)
		}
	}
}

func TestReverse(t *testing.T) {
	if testing.Short() {
		t.Skip("Reverse DNS test")
	}
	validServer := net.IPv4(116, 203, 3, 137)
	expectName := "mail.alienscience.org"
	m := NewMxDNS(ipBlocklists, domainBlocklists)
	fcrdns, host := m.CheckReverse(validServer, expectName)
	if !fcrdns {
		t.Errorf("%s failed FCRDNS check", validServer.String())
	}
	if !host {
		t.Errorf("%s failed reverse DNS check", validServer.String())
	}
}

package smtp

// EmptyTracer turns off SMTP tracing -- this is the default.
var EmptyTracer = emptyTracer{}

// A SessionTracer is used to trace a single SMTP session.
type SessionTracer interface {
	// Data from client
	Client([]byte)
	// Data from server
	Server([]byte)
	// Server sends data to client
	ServerFlush()
	// End of smtp session
	Close()
}

type emptyTracer struct{}

// Client is called when the server receives data from the client
func (t emptyTracer) Client([]byte) {}

// Server is called when the server builds a response
func (t emptyTracer) Server([]byte) {}

// ServerFlush is called when the server sends responses to the client
func (t emptyTracer) ServerFlush() {}

// Close is called when the SMTP session finishes
func (t emptyTracer) Close() {}

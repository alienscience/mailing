package smtp

import (
	"bufio"
	"bytes"
	"io"
	"io/ioutil"
	"net"
	"path/filepath"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/alienscience/mailing/internal/testutil"
)

type helloHandler func(ip net.IP, fqdn string) *Response
type mailFromHandler func(ip net.IP, fqdn, reversePath string) *Response
type rcptToHandler func(fqdn, reversePath, recipient string) *Response
type dataHandler func(reversePath string, recipients []string) (io.WriteCloser, *Response)

// A SessionHandler to testing example sessions
type handlers struct {
	hello    helloHandler
	mailFrom mailFromHandler
	rcptTo   rcptToHandler
	data     dataHandler
}

var expectedEhloTLS = []string{"mx.domain.com EHLO", "8BITMIME", "SMTPUTF8", "PIPELINING", "STARTTLS"}
var expectedEhloNoTLS = []string{"mx.domain.com EHLO", "8BITMIME", "SMTPUTF8", "PIPELINING"}

// A SessionHandler that captures all incoming data
type testHandler struct {
	ip          net.IP
	domain      string
	reversePath string
	recipients  []string
	data        *bytes.Buffer
}

type writeNopCloser struct {
	io.Writer
}

func (writeNopCloser) Close() error { return nil }

func (h *handlers) Session() (SessionHandler, SessionTracer) {
	return h, EmptyTracer
}

func (h *handlers) Hello(ip net.IP, fqdn string) *Response {
	if h.hello == nil {
		return OkReply
	}
	return h.hello(ip, fqdn)
}

func (h *handlers) MailFrom(ip net.IP, fqdn, reversePath string) *Response {
	if h.mailFrom == nil {
		return OkReply
	}
	return h.mailFrom(ip, fqdn, reversePath)
}

func (h *handlers) RcptTo(fqdn, reversePath, recipient string) *Response {
	if h.rcptTo == nil {
		return OkReply
	}
	return h.rcptTo(fqdn, reversePath, recipient)
}

func (h *handlers) Data(reversePath string, recipients []string) (io.WriteCloser, *Response) {
	if h.data == nil {
		writer := writeNopCloser{ioutil.Discard}
		return writer, OkReply
	}
	return h.data(reversePath, recipients)
}

func (h *handlers) End() {}

func newTestHandler() *testHandler {
	return &testHandler{data: new(bytes.Buffer)}
}

func (h *testHandler) Session() (SessionHandler, SessionTracer) {
	return h, EmptyTracer
}

func (h *testHandler) Hello(ip net.IP, domain string) *Response {
	h.ip = ip
	h.domain = domain
	return OkReply
}

func (h *testHandler) MailFrom(ip net.IP, domain, reversePath string) *Response {
	h.ip = ip
	h.domain = domain
	h.reversePath = reversePath
	return OkReply
}

func (h *testHandler) RcptTo(domain, reversePath, recipient string) *Response {
	h.domain = domain
	h.reversePath = reversePath
	h.recipients = append(h.recipients, recipient)
	return OkReply
}

func (h *testHandler) Data(reversePath string, recipients []string) (io.WriteCloser, *Response) {
	return writeNopCloser{h.data}, OkReply
}

func (h *testHandler) End() {}

func connect(t *testing.T) (*testHandler, *session) {
	handler := newTestHandler()
	ip := net.IPv4(127, 0, 0, 1)
	server := serverConfig{"mx.domain.com", startTLSExt, handler, nil}
	session := newSession(server, ip)
	res := session.greeting()
	if !res.IsOk() {
		t.Fail()
	}
	return handler, session
}

func TestConnect(t *testing.T) {
	connect(t)
}

func TestHelo(t *testing.T) {
	handler, session := connect(t)
	res := session.recv([]byte("HELO domain.com\r\n"))
	if !(res.IsOk() && handler.domain == "domain.com") || res.CanBuffer {
		t.Fail()
	}
}

func TestEhlo(t *testing.T) {
	handler, session := connect(t)
	res := session.recv([]byte("EHLO domain.com\r\n"))
	if !(res.IsOk() && handler.domain == "domain.com") || res.CanBuffer {
		t.Fail()
	}
	if !cmp.Equal(res.Lines, expectedEhloTLS) {
		t.Error(res.Lines)
	}
}

func TestStartTls(t *testing.T) {
	_, session := connect(t)
	res := session.recv([]byte("EHLO domain.com\r\n"))
	if !res.IsOk() || res.CanBuffer {
		t.Fail()
	}
	res = session.recv([]byte("STARTTLS\r\n"))
	if !res.IsOk() || res.CanBuffer {
		t.Fail()
	}
	session.startTLSOk()
	if _, ok := session.state.(*idleState); !ok {
		t.Fail()
	}
	res = session.recv([]byte("EHLO domain.com\r\n"))
	if !res.IsOk() || res.CanBuffer {
		t.Fail()
	}
	if !cmp.Equal(res.Lines, expectedEhloNoTLS) {
		t.Error(res.Lines)
	}
}

func TestStartTlsFail(t *testing.T) {
	_, session := connect(t)
	res := session.recv([]byte("EHLO domain.com\r\n"))
	if !res.IsOk() || res.CanBuffer {
		t.Fail()
	}
	res = session.recv([]byte("STARTTLS\r\n"))
	if !res.IsOk() || res.CanBuffer {
		t.Fail()
	}
	// No StartTlsOk()
	res = session.recv([]byte("EHLO domain.com\r\n"))
	if !res.IsOk() || res.CanBuffer {
		t.Fail()
	}
	if !cmp.Equal(res.Lines, expectedEhloTLS) {
		t.Error(res.Lines)
	}
}

func TestMailFrom(t *testing.T) {
	handler, session := connect(t)
	_ = session.recv([]byte("EHLO domain.com\r\n"))
	res := session.recv([]byte("MAIL FROM: <fish@sea.com>\r\n"))
	if !(res.IsOk() && res.CanBuffer) || handler.reversePath != "fish@sea.com" {
		t.Fail()
	}
}

func TestRcptTo(t *testing.T) {
	handler, session := connect(t)
	_ = session.recv([]byte("EHLO domain.com\r\n"))
	_ = session.recv([]byte("MAIL FROM: <fish@sea.com>\r\n"))
	res := session.recv([]byte("RCPT TO: <shark@ocean.org>\r\n"))
	if !(res.IsOk() && res.CanBuffer) {
		t.Fail()
	}
	res = session.recv([]byte("RCPT TO: <cow@field.com>\r\n"))
	if !(res.IsOk() && res.CanBuffer) {
		t.Fail()
	}
	if !cmp.Equal(handler.recipients,
		[]string{"shark@ocean.org", "cow@field.com"}) {
		t.Fail()
	}
}

func TestData(t *testing.T) {
	handler, session := connect(t)
	_ = session.recv([]byte("EHLO domain.com\r\n"))
	_ = session.recv([]byte("MAIL FROM: <fish@sea.com>\r\n"))
	_ = session.recv([]byte("RCPT TO: <shark@ocean.org>\r\n"))
	res := session.recv([]byte("DATA\r\n"))
	if !res.IsOk() || res.CanBuffer {
		t.Fail()
	}
	if !cmp.Equal(handler.recipients, []string{"shark@ocean.org"}) {
		t.Fail()
	}
	res = session.recv([]byte("data is opaque\r\n"))
	if res != EmptyReply {
		t.Fail()
	}
	res = session.recv([]byte("..dot stuffing is supported\r\n"))
	if res != EmptyReply {
		t.Fail()
	}
	res = session.recv([]byte(".\r\n"))
	if !(res.IsOk() || res.CanBuffer) {
		t.Fail()
	}
	if handler.reversePath != "fish@sea.com" {
		t.Fail()
	}
	if !cmp.Equal(handler.recipients, []string{"shark@ocean.org"}) {
		t.Fail()
	}
	data := handler.data.Bytes()
	if !cmp.Equal(data, []byte("data is opaque\r\n.dot stuffing is supported\r\n")) {
		t.Error(string(data))
	}
}

func TestExampleSessions(t *testing.T) {
	files, err := ioutil.ReadDir("testdata")
	if err != nil {
		t.Fatal(err)
	}
	for _, file := range files {
		name := file.Name()
		if strings.HasSuffix(name, ".smtp") {
			path := filepath.Join("testdata", name)
			t.Run(name, func(t *testing.T) {
				runSessionTest(t, path)
			})
		}
	}
}

func runSessionTest(t *testing.T, path string) {
	hs := &handlers{
		rcptTo: func(fqdn, reversePath, recipient string) *Response {
			if recipient == "Smith@bar.com" ||
				recipient == "Jones@foo.com" ||
				recipient == "Brown@foo.com" {
				return OkReply
			}
			return NoSuchUser
		},
	}
	ip := net.IPv4(127, 0, 0, 1)
	server := serverConfig{"foo.com", startTLSExt, hs, nil}
	session := newSession(server, ip)
	rdr := bufio.NewReader(testutil.CrReader(t, path))
	simulateSession(t, rdr, session)
}

func simulateSession(t *testing.T, rdr *bufio.Reader, session *session) {
	expectClient := true
	responses := make(chan []byte, 8)
	responses <- session.greeting().bytes()
	for {
		line, err := rdr.ReadSlice('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			t.Fatal(err)
		}
		switch {
		case bytes.HasPrefix(line, []byte("C: ")):
			if expectClient {
				r := session.recv(line[3:])
				expectClient = r.CanBuffer || r == EmptyReply
				addResponse(responses, r)
				if r == StartTLSReply {
					session.startTLSOk()
				}
			} else {
				t.Fatalf("Unexpected Client activity: %s", line)
			}
		case bytes.HasPrefix(line, []byte("S: ")):
			expectClient = true
			res := <-responses
			if !cmp.Equal(res, line[3:]) {
				t.Fatalf("%s != %s", res, line[3:])
			}
		default:
			t.Fatalf("Invalid test line: %s", line)
		}
	}
}

func addResponse(responses chan<- []byte, response *Response) {
	responseLines := bytes.SplitAfter(response.bytes(), []byte("\r\n"))
	for _, l := range responseLines {
		if len(l) > 0 {
			responses <- l
		}
	}
}

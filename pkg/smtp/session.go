package smtp

import (
	"bufio"
	"fmt"
	"net"
)

// Extensions that can be enabled/disabled
const (
	startTLSExt = 1 << iota
	authPlainExt
)

// Extensions that are always enabled
var extensions = []string{"8BITMIME", "SMTPUTF8", "PIPELINING"}

type session struct {
	serverConfig
	handler        SessionHandler
	ip             net.IP
	state          state
	startTLSActive bool
	trace          SessionTracer
}

type state interface {
	recv(*session, []byte) *Response
}

func newSession(server serverConfig, ip net.IP) *session {
	handler, trace := server.handler.Session()
	return &session{
		serverConfig:   server,
		handler:        handler,
		ip:             ip,
		state:          &idleState{},
		startTLSActive: false,
		trace:          trace,
	}
}

func (s *session) greeting() *Response {
	greet := s.connectGreeting()
	s.trace.Server(greet.bytes())
	s.trace.ServerFlush()
	return greet
}

// Handle a session communicating over the given ReadWriter. Returns true
// if the session should be restarted with TLS active. If the session is
// restarted the ReadWriter should wrap a TLS connection.
func (s *session) run(rw *bufio.ReadWriter) bool {
	for {
		line, err := rw.Reader.ReadBytes('\n')
		if err != nil {
			break
		}
		res := s.recv(line)
		if res == EmptyReply {
			continue
		}
		reply := res.bytes()
		_, err = rw.Writer.Write(reply)
		if err != nil {
			break
		}
		s.trace.Server(reply)
		// Pipelining support - if the response can be buffered AND
		// there is still data left to read, do not send the response
		if !(res.CanBuffer && rw.Reader.Buffered() > 0) {
			err = rw.Writer.Flush()
			if err != nil {
				break
			}
			s.trace.ServerFlush()
		}
		// STARTTLS support
		if res == StartTLSReply {
			return true
		}
	}
	s.trace.Close()
	s.handler.End()
	return false // Do not Start Tls
}

// Recv takes a line of input from a client and returns a response.
func (s *session) recv(b []byte) *Response {
	return s.state.recv(s, b)
}

func (s *session) startTLSOk() {
	s.startTLSActive = true
	s.state = &idleState{}
}

func (s *session) connectGreeting() *Response {
	return &Response{
		Code:  220,
		Lines: []string{fmt.Sprint(s.fqdn, " ESMTP Ready")},
	}
}

func (s *session) heloGreeting() *Response {
	return &Response{
		Code:  250,
		Lines: []string{fmt.Sprint(s.fqdn, " Helo")},
	}
}

func (s *session) ehloGreeting() *Response {
	lines := []string{fmt.Sprint(s.fqdn, " EHLO")}
	lines = append(lines, extensions...)
	res := &Response{Code: 250, Lines: lines}
	if s.extensions&startTLSExt != 0 && !s.startTLSActive {
		res.Lines = append(res.Lines, "STARTTLS")
	}
	if s.extensions&authPlainExt != 0 && s.startTLSActive {
		res.Lines = append(res.Lines, "AUTH PLAIN")
	}
	return res
}

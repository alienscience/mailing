package smtp

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

type cmdTest struct {
	in      string
	cmd     cmd
	isError bool
}

var cmdTests = []cmdTest{
	{in: "HELO domain.com", cmd: cmd{id: heloCmd, arg: "domain.com"}},
	{in: "HELO domain.x-y", cmd: cmd{id: heloCmd, arg: "domain.x-y"}},
	{in: "HELO", isError: true},
	{in: "HELO -not-a-domain-", isError: true},
	{in: "EHLO WIN-CLJ1B0GQ6JP", cmd: cmd{id: ehloCmd, arg: "WIN-CLJ1B0GQ6JP"}},
	{in: "ehlo WIN-CLJ1B0GQ6JP", isError: true},
	{in: "EHLO domain.com", cmd: cmd{id: ehloCmd, arg: "domain.com"}},
	{in: "EHLO [192.168.2.1]", cmd: cmd{id: ehloCmd, arg: "[192.168.2.1]"}},
	{in: "EHLO ", isError: true},
	{in: "MAIL FROM: <hello@domain.com>", cmd: cmd{id: mailFromCmd, arg: "hello@domain.com"}},
	{in: "MAIL FROM:<hello@domain.com>", cmd: cmd{id: mailFromCmd, arg: "hello@domain.com"}},
	{in: "MAIL FROM: <hello@domain.com> BODY=8BITMIME",
		cmd: cmd{id: mailFromCmd, arg: "hello@domain.com", params: map[string]string{"BODY": "8BITMIME"}}},
	{in: "MAIL FROM: <hello@some-domain.com>", cmd: cmd{id: mailFromCmd, arg: "hello@some-domain.com"}},
	{in: "MAIL FROM:  <hello@domain.com>", cmd: cmd{id: mailFromCmd, arg: "hello@domain.com"}},
	{in: "MAIL FROM: <hello@domain.x-y>", cmd: cmd{id: mailFromCmd, arg: "hello@domain.x-y"}},
	{in: "MAIL FROM: <>", cmd: cmd{id: mailFromCmd, arg: ""}},
	{in: "MAIL FROM: <hello@domain.co->", isError: true},
	{in: "MAIL FROM: hello@domain.com>", isError: true},
	{in: "MAIL FROM: <hello@domain.com", isError: true},
	{in: "MAIL FROM: <@one,@two:hello@domain.com>", cmd: cmd{id: mailFromCmd, arg: "hello@domain.com"}},
	{in: "MAIL FROM: <wikipedians+bob=example.org@wiki.net>",
		cmd: cmd{id: mailFromCmd, arg: "wikipedians+bob=example.org@wiki.net"}},
	{in: "MAIL FROM: <jsmith@[192.168.2.1]>", cmd: cmd{id: mailFromCmd, arg: "jsmith@[192.168.2.1]"}},
	{in: "MAIL FROM: <jsmith@[IPv6:2001:db8::1]>", cmd: cmd{id: mailFromCmd, arg: "jsmith@[IPv6:2001:db8::1]"}},
	{in: "MAIL FROM: <hello@öbb.at>", cmd: cmd{id: mailFromCmd, arg: "hello@öbb.at"}},
	{in: "MAIL FROM: <hello@faß.de> SMTPUTF8",
		cmd: cmd{id: mailFromCmd, arg: "hello@faß.de", params: map[string]string{"SMTPUTF8": ""}}},
	{in: "MAIL FROM: <über@domain.com>", cmd: cmd{id: mailFromCmd, arg: "über@domain.com"}},
	{in: "MAIL FROM: <über@öbb.at>", cmd: cmd{id: mailFromCmd, arg: "über@öbb.at"}},
	{in: "RCPT TO: <hello@some-domain.com>", cmd: cmd{id: rcptToCmd, arg: "hello@some-domain.com"}},
	{in: "RCPT TO: <Postmaster@domain.com>", cmd: cmd{id: rcptToCmd, arg: "Postmaster@domain.com"}},
	{in: "RCPT TO: <Postmaster>", cmd: cmd{id: rcptToCmd, arg: "Postmaster"}},
	{in: "DATA", cmd: cmd{id: dataCmd}},
	{in: "RSET", cmd: cmd{id: rsetCmd}},
	{in: "VRFY smith", cmd: cmd{id: vrfyCmd, arg: "smith"}},
	{in: "VRFY <hello@domain.com>", cmd: cmd{id: vrfyCmd, arg: "<hello@domain.com>"}},
	{in: "EXPN some-list", cmd: cmd{id: expnCmd, arg: "some-list"}},
	{in: "HELP", cmd: cmd{id: helpCmd}},
	{in: "HELP RSET", cmd: cmd{id: helpCmd, arg: "RSET"}},
	{in: "NOOP", cmd: cmd{id: noopCmd}},
	{in: "NOOP some arg", cmd: cmd{id: noopCmd, arg: "some arg"}},
	{in: "QUIT", cmd: cmd{id: quitCmd}},
}

func TestCmd(t *testing.T) {
	for _, testCase := range cmdTests {
		tc := testCase
		t.Run(testCase.in, func(t *testing.T) {
			cmd, err := parseCommand([]byte(tc.in))
			switch {
			case tc.isError:
				if err == nil {
					t.Error("Expected an error but got none for", tc.in)
				}
			case err != nil:
				t.Error(tc.in, err)
			case !cmp.Equal(
				cmd,
				tc.cmd,
				cmpopts.EquateEmpty(), cmp.AllowUnexported(cmd)):
				t.Errorf("Expected %#v got %#v", tc.cmd, cmd)
			}
		})
	}
}

func BenchmarkCmdParseNewParser(b *testing.B) {
	for n := 0; n < b.N; n++ {
		for _, testCase := range cmdTests {
			_, _ = parseCommand([]byte(testCase.in))
		}
	}
}

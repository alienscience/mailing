package smtp

import (
	"io"
	"log"
)

type idleState struct{}

func (s *idleState) recv(session *session, b []byte) *Response {
	session.trace.Client(b)
	c, err := parseCommand(b)
	if err != nil {
		return SyntaxError
	}
	switch c.id {
	case heloCmd, ehloCmd:
		return handleHello(c, session, c.arg)
	case rsetCmd:
		return OkReply
	default:
		return handleDefaults(c, session)
	}
}

type exitState struct{}

func (s exitState) recv(session *session, b []byte) *Response {
	return BadSequenceCommands
}

type helloState struct {
	domain string
}

func (s helloState) recv(session *session, b []byte) *Response {
	session.trace.Client(b)
	c, err := parseCommand(b)
	if err != nil {
		return SyntaxError
	}
	switch c.id {
	case heloCmd, ehloCmd:
		return handleHello(c, session, c.arg)
	case startTLSCmd:
		if session.extensions&startTLSExt == 0 {
			return CommandNotImplemented
		}
		return StartTLSReply
	case mailFromCmd:
		reversePath := c.arg
		res := session.handler.MailFrom(session.ip, s.domain, reversePath)
		if !res.IsOk() {
			return res
		}
		session.state = mailState{s.domain, reversePath}
		return OkReply
	case rsetCmd:
		return OkReply
	default:
		return handleDefaults(c, session)
	}
}

type mailState struct {
	domain      string
	reversePath string
}

func (s mailState) recv(session *session, b []byte) *Response {
	session.trace.Client(b)
	c, err := parseCommand(b)
	if err != nil {
		return SyntaxError
	}
	switch c.id {
	case heloCmd, ehloCmd:
		return handleHello(c, session, c.arg)
	case rsetCmd:
		session.state = helloState{s.domain}
		return OkReply
	case rcptToCmd:
		recipient := c.arg
		res := session.handler.RcptTo(s.domain, s.reversePath, recipient)
		nextState := &rcptState{s.domain, s.reversePath, make([]string, 0, 4)}
		if !res.IsOk() {
			// To support pipelining we must be prepared to accept DATA even
			// if there are no valid recipients
			session.state = nextState
			return res
		}
		nextState.recipients = append(nextState.recipients, recipient)
		session.state = nextState
		return OkReply
	default:
		return handleDefaults(c, session)
	}
}

type rcptState struct {
	domain      string
	reversePath string
	recipients  []string
}

func (s *rcptState) recv(session *session, b []byte) *Response {
	session.trace.Client(b)
	c, err := parseCommand(b)
	if err != nil {
		return SyntaxError
	}
	switch c.id {
	case heloCmd, ehloCmd:
		return handleHello(c, session, c.arg)
	case rsetCmd:
		session.state = helloState{s.domain}
		return OkReply
	case rcptToCmd:
		recipient := c.arg
		res := session.handler.RcptTo(s.domain, s.reversePath, recipient)
		if !res.IsOk() {
			return res
		}
		s.recipients = append(s.recipients, c.arg)
		return OkReply
	case dataCmd:
		if len(s.recipients) == 0 {
			return NoValidRecipients
		}
		wr, res := session.handler.Data(s.reversePath, s.recipients)
		if !res.IsOk() {
			return res
		}
		session.state = &dataState{s.domain, s.reversePath, s.recipients, wr, false}
		return StartMailInput
	default:
		return handleDefaults(c, session)
	}
}

type dataState struct {
	domain      string
	reversePath string
	recipients  []string
	writer      io.WriteCloser
	isError     bool
}

func (s *dataState) recv(session *session, b []byte) *Response {
	if b[0] == '.' && b[1] == '\r' && b[2] == '\n' {
		session.trace.Client(b)
		session.state = helloState{s.domain}
		err := s.writer.Close()
		if err != nil || s.isError {
			return LocalError
		}
		return OkReply
	}
	line := b
	if line[0] == '.' {
		line = line[1:]
	}
	if !s.isError {
		_, err := s.writer.Write(line)
		if err != nil {
			log.Print("DATA RECV error: ", err)
			s.isError = true
		}
	}
	return EmptyReply
}

func handleHello(c cmd, session *session, domain string) *Response {
	switch c.id {
	case heloCmd:
		res := session.handler.Hello(session.ip, domain)
		if !res.IsOk() {
			return res
		}
		session.state = helloState{domain}
		return session.heloGreeting()
	case ehloCmd:
		res := session.handler.Hello(session.ip, domain)
		if !res.IsOk() {
			return res
		}
		session.state = helloState{domain}
		return session.ehloGreeting()
	default:
		return handleDefaults(c, session)
	}
}

func handleDefaults(c cmd, session *session) *Response {
	switch c.id {
	case noopCmd:
		return OkReply
	case quitCmd:
		session.state = exitState{}
		return QuitReply
	case vrfyCmd:
		return VrfyNotImplemented
	case expnCmd:
		return CommandNotImplemented
	case helpCmd:
		return CommandNotImplemented
	default:
		return BadSequenceCommands
	}
}

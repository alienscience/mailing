package smtp

// Mail commands
const (
	heloCmd = iota
	ehloCmd
	mailFromCmd
	rcptToCmd
	dataCmd
	rsetCmd
	vrfyCmd
	expnCmd
	helpCmd
	noopCmd
	quitCmd
	startTLSCmd
	endOfCmd // Mark the last command
)

type cmd struct {
	id     int
	arg    string
	params map[string]string
}

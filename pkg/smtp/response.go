package smtp

import (
	"bytes"
	"fmt"
)

// Response contains the unserialized response to an SMTP command.
type Response struct {
	Code      uint16
	Lines     []string
	CanBuffer bool
}

func (r *Response) bytes() []byte {
	var b bytes.Buffer
	last := len(r.Lines) - 1
	for i, line := range r.Lines {
		if i == last {
			_, _ = b.WriteString(fmt.Sprintf("%d %s\r\n", r.Code, line))
		} else {
			_, _ = b.WriteString(fmt.Sprintf("%d-%s\r\n", r.Code, line))
		}
	}
	return b.Bytes()
}

// IsOk indicates if a Response is a successful response.
func (r *Response) IsOk() bool {
	return r.Code < 400
}

var (
	// EmptyReply is a response that is not sent to the client.
	EmptyReply = &Response{0, nil, true}

	// StartTLSReply is returned when TLS negotiation should be started.
	StartTLSReply = &Response{220, []string{"Ready to start TLS"}, false}

	// QuitReply is returned when the connection to the client should be closed.
	QuitReply = &Response{221, []string{"Closing transmission channel"}, false}

	// OkReply is returned when an SMTP command is successful.
	OkReply = &Response{250, []string{"OK"}, true}

	// VrfyNotImplemented is returned in response to a VRFY command
	VrfyNotImplemented = &Response{252, []string{"VRFY not implemented"}, true}

	// StartMailInput is returned when the server is ready to receive a mail message.
	StartMailInput = &Response{354, []string{"Start mail input"}, false}

	// ServiceNotAvailable is returned when the server is out of order.
	ServiceNotAvailable = &Response{421, []string{"Service not available, closing transmission channel"}, false}

	// LocalError is returned when an internal error occurs.
	LocalError = &Response{451, []string{"Action aborted, local error in processing"}, false}

	// SyntaxError is returned when the SMTP parser detects an error.
	SyntaxError = &Response{500, []string{"Syntax error"}, false}

	// ParameterSyntaxError is returned when the SMTP parser detects and error with a command parameter.
	ParameterSyntaxError = &Response{501, []string{"Syntax error"}, false}

	// CommandNotImplemented is returned when an unsupported SMTP command is received.
	CommandNotImplemented = &Response{502, []string{"Command not implemented"}, false}

	// BadSequenceCommands is returned when a supported but unexpected command arrives.
	BadSequenceCommands = &Response{503, []string{"Bad sequence of commands"}, false}

	// NoSuchUser is returned when the receipient is unknown.
	NoSuchUser = &Response{550, []string{"No such user"}, true}

	// BadHello is returned when the client identification does not match its reverse DNS lookup.
	BadHello = &Response{550, []string{"Bad HELO"}, true}

	// BadFcrdns is returned when the client reverse DNS lookup does not
	// resolve to the client IP address.
	BadFcrdns = &Response{550, []string{"FCRDNS failure"}, true}

	// IPOnBlocklist is returned when the client IP is on an SMTP blocklist.
	IPOnBlocklist = &Response{554, []string{"IP on blocklist"}, true}

	// DomainOnBlocklist is returned when a domain is on an SMTP blocklist.
	DomainOnBlocklist = &Response{554, []string{"Domain on blocklist"}, true}

	// NoValidRecipients is returned during pipelining when a DATA command
	// is received but no valid recipients are available
	NoValidRecipients = &Response{554, []string{"No valid recipients given"}, false}
)

// Package smtp provides a configurable SMTP server.
package smtp

import (
	"bufio"
	"crypto/tls"
	"errors"
	"log"
	"net"
)

// Server is an SMTP server.
type Server struct {
	serverConfig
	listener net.Listener
}

// An OptionFn is used to configure the SMTP server.
type OptionFn func(s *Server) error

type serverConfig struct {
	fqdn       string
	extensions uint8
	handler    Handler
	tlsConfig  *tls.Config
}

// WithFqdn sets the Fully Qualified Domain Name of the Server.
func WithFqdn(fqdn string) OptionFn {
	return func(s *Server) error {
		s.fqdn = fqdn
		return nil
	}
}

// WithAddress sets the SMTP address and port of the Server.
func WithAddress(address string) OptionFn {
	return func(s *Server) error {
		listener, err := net.Listen("tcp", address)
		if err == nil {
			s.listener = listener
		}
		return err
	}
}

// WithTLS sets the TLS configuration of the Server and enables the STARTTLS ESMTP extension.
func WithTLS(certFile, keyFile string) OptionFn {
	return func(s *Server) error {
		var err error
		tlsConfig := &tls.Config{MinVersion: tls.VersionTLS12}
		tlsConfig.Certificates = make([]tls.Certificate, 1)
		tlsConfig.Certificates[0], err = tls.LoadX509KeyPair(certFile, keyFile)
		if err == nil {
			s.tlsConfig = tlsConfig
			s.extensions |= startTLSExt
		}
		return err
	}
}

// WithHandler sets a struct that will handle SMTP callbacks.
func WithHandler(h Handler) OptionFn {
	return func(s *Server) error {
		s.handler = h
		return nil
	}
}

// NewServer creates a new SMTP server.
func NewServer(options ...OptionFn) (*Server, error) {
	srv := &Server{
		serverConfig: serverConfig{
			fqdn: "localhost",
		},
	}
	for _, o := range options {
		err := o(srv)
		if err != nil {
			return nil, err
		}
	}

	// Check configured server
	if srv.listener == nil {
		return nil, errors.New("no listener configured")
	}
	if srv.handler == nil {
		return nil, errors.New("no handler configured")
	}
	return srv, nil
}

// ListenAndServe starts an SMTP and does not return.
func (s *Server) ListenAndServe() {
	for {
		client, err := s.listener.Accept()
		if err != nil {
			log.Println("Could not accept: ", err)
			continue
		}
		ip, err := ipFromAddr(client.RemoteAddr().String())
		if err != nil {
			log.Println("Bad remote address: ", client.RemoteAddr())
			continue
		}
		session := newSession(s.serverConfig, ip)
		if err != nil {
			log.Println("Could not create SessionHandler: ", err)
			continue
		}
		go s.clientSession(client, session)
	}
}

func (s *Server) clientSession(client net.Conn, session *session) {
	defer func() { _ = client.Close() }()
	greeting := session.greeting().bytes()
	_, err := client.Write(greeting)
	if err != nil {
		return
	}
	rw := bufio.NewReadWriter(bufio.NewReader(client), bufio.NewWriter(client))
	startTLS := session.run(rw)
	if startTLS {
		rw, err = s.startTLS(client, session)
		if err != nil {
			log.Println(err)
			return
		}
		session.run(rw)
	}
}

func (s *Server) startTLS(clientConn net.Conn, session *session) (*bufio.ReadWriter, error) {
	tlsConn := tls.Server(clientConn, s.tlsConfig)
	err := tlsConn.Handshake()
	if err != nil {
		return bufio.NewReadWriter(
				bufio.NewReader(clientConn),
				bufio.NewWriter(clientConn)),
			err
	}
	session.startTLSOk()
	reader := bufio.NewReader(tlsConn)
	writer := bufio.NewWriter(tlsConn)
	return bufio.NewReadWriter(reader, writer), nil
}

func ipFromAddr(addr string) (net.IP, error) {
	host, _, err := net.SplitHostPort(addr)
	if err != nil {
		return nil, err
	}
	return net.ParseIP(host), nil
}

package smtp

import (
	"io"
	"net"
)

// A Handler is called by the smtp server at the start of every SMTP session. The
// Handler is expected to hold values that are common to all SMTP sessions e.g
// database connections.
type Handler interface {
	// Session should return a SessionHandler that will be used to control
	// a single SMTP session and a SessionTracer that can be used to trace
	// the I/O the SMTP session.
	Session() (SessionHandler, SessionTracer)
}

// A SessionHandler is called by the smtp server during an SMTP session. Structs
// that implement SessionHandler can hold state that is specific to a single session.
type SessionHandler interface {
	// Hello is called when the server receives a client HELO or EHLO. It
	// is expected that this function checks that the fqdn and ip address
	// are valid.
	Hello(ip net.IP, fqdn string) *Response
	// MailFrom is called when the server receives a MAIL command
	MailFrom(ip net.IP, fqdn, reversePath string) *Response
	// RcptTo is called when the server receives a RCPT command. It is expected
	// that this function checks that the recipient is valid.
	RcptTo(fqdn, reversePath, recipient string) *Response
	// Data is called when the server receives message data. This function should
	// return a WriteCloser that the SMTP server will write the message data to.
	Data(reversePath string, recipients []string) (io.WriteCloser, *Response)
	// End is called when the SMTP session has ended
	End()
}

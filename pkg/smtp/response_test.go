package smtp

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestResponseString(t *testing.T) {
	if !cmp.Equal(OkReply.bytes(), []byte("250 OK\r\n")) {
		t.Fail()
	}
	multiline := &Response{
		Code: 250,
		Lines: []string{
			"First line",
			"Second line",
			"234 Text beginning with numbers",
			"The last line",
		}}
	if !cmp.Equal(multiline.bytes(),
		[]byte("250-First line\r\n"+
			"250-Second line\r\n"+
			"250-234 Text beginning with numbers\r\n"+
			"250 The last line\r\n")) {
		t.Fail()
	}
}

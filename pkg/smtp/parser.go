package smtp

import (
	"errors"

	"gitlab.com/alienscience/parser"
)

var (
	cmdParser       = newCmdParser()
	mboxPartsParser = mailboxParts()
)

const (
	argToken = endOfCmd + iota
	paramKeyToken
	paramValueToken
	localPartToken
	domainToken
	addrLiteralToken
)

// MailboxParts contains a parsed mailbox that has been split into parts.
type MailboxParts struct {
	Local       string
	Domain      string
	AddrLiteral string
}

func parseCommand(b []byte) (cmd, error) {
	in := parser.NewState(b)
	res := cmdParser.Parse(in)
	if !res.Success {
		return cmd{}, errors.New("parser failure")
	}
	ret := cmd{}
	key := ""
	tokens := res.TokenStream()
	for !tokens.Match(parser.EOFToken) {
		token := tokens.Next()
		switch {
		case token.ID < endOfCmd:
			ret.id = token.ID
		case token.ID == argToken:
			ret.arg = string(token.Capture)
		case token.ID == paramKeyToken:
			key = string(token.Capture)
			if ret.params == nil {
				ret.params = make(map[string]string, 2)
			}
			ret.params[key] = ""
		case token.ID == paramValueToken:
			ret.params[key] = string(token.Capture)
		}
	}
	return ret, nil
}

// ParseMailbox parses a mailbox string into local and domain parts.
func ParseMailbox(mbox string) (MailboxParts, error) {
	in := parser.NewState([]byte(mbox))
	res := mboxPartsParser.Parse(in)
	if !res.Success {
		return MailboxParts{}, errors.New("parser failure")
	}
	ret := MailboxParts{}
	tokens := res.TokenStream()
	for !tokens.Match(parser.EOFToken) {
		token := tokens.Next()
		switch token.ID {
		case localPartToken:
			ret.Local = string(token.Capture)
		case domainToken:
			ret.Domain = string(token.Capture)
		case addrLiteralToken:
			ret.AddrLiteral = string(token.Capture)
		}
	}
	return ret, nil
}

func newCmdParser() parser.Fn {
	return parser.OneOf(
		helo(),
		ehlo(),
		mailFrom(),
		rcptTo(),
		data(),
		rset(),
		vrfy(),
		expn(),
		help(),
		noop(),
		quit(),
		startTLS(),
	)
}

// helo = "HELO" SP Domain CRLF
func helo() parser.Fn {
	return parser.Seq(
		parser.Emit(
			parser.Lit("HELO"),
			heloCmd,
		),
		sp(),
		parser.Emit(
			domain(),
			argToken,
		),
	)
}

// ehlo = "EHLO" SP ( Domain / address-literal ) CRLF
func ehlo() parser.Fn {
	return parser.Seq(
		parser.Emit(
			parser.Lit("EHLO"),
			ehloCmd,
		),
		sp(),
		parser.Emit(
			parser.OneOf(domain(), addrLiteral()),
			argToken,
		),
	)
}

// mailFrom <- "mail from:" sp reversePath
func mailFrom() parser.Fn {
	return parser.Seq(
		parser.Emit(
			parser.Lit("MAIL FROM:"),
			mailFromCmd,
		),
		sp(),
		reversePath(),
		parser.Optional(parser.Seq(sp(), mailParams())),
	)
}

// rcpt = "RCPT TO:" ( "<Postmaster@" Domain ">" / "<Postmaster>" /
//           Forward-path ) [SP Rcpt-parameters] CRLF
func rcptTo() parser.Fn {
	return parser.Seq(
		parser.Emit(
			parser.Lit("RCPT TO:"),
			rcptToCmd,
		),
		sp(),
		parser.OneOf(
			postmaster(),
			forwardPath(),
		),
		parser.Optional(parser.Seq(sp(), rcptParams())),
	)
}

// data = "DATA" CRLF
func data() parser.Fn {
	return parser.Emit(
		parser.Lit("DATA"),
		dataCmd,
	)
}

// rset = "RSET" CRLF
func rset() parser.Fn {
	return parser.Emit(
		parser.Lit("RSET"),
		rsetCmd,
	)
}

// vrfy = "VRFY" SP String CRLF
func vrfy() parser.Fn {
	return parser.Seq(
		parser.Emit(
			parser.Lit("VRFY"),
			vrfyCmd,
		),
		sp(),
		strArg(),
	)
}

// expn = "EXPN" SP String CRLF
func expn() parser.Fn {
	return parser.Seq(
		parser.Emit(
			parser.Lit("EXPN"),
			expnCmd,
		),
		sp(),
		strArg(),
	)
}

// help = "HELP" [ SP String ] CRLF
func help() parser.Fn {
	return parser.Seq(
		parser.Emit(
			parser.Lit("HELP"),
			helpCmd,
		),
		parser.Optional(
			parser.Seq(sp(), strArg())),
	)
}

// noop = "NOOP" [ SP String ] CRLF
func noop() parser.Fn {
	return parser.Seq(
		parser.Emit(
			parser.Lit("NOOP"),
			noopCmd,
		),
		parser.Optional(
			parser.Seq(sp(), strArg())),
	)
}

// quit = "QUIT" CRLF
func quit() parser.Fn {
	return parser.Emit(
		parser.Lit("QUIT"),
		quitCmd,
	)
}

// STARTTLS
func startTLS() parser.Fn {
	return parser.Emit(
		parser.Lit("STARTTLS"),
		startTLSCmd,
	)
}

// reversePath <- path / "<>"
func reversePath() parser.Fn {
	return parser.OneOf(
		path(), parser.Lit("<>"),
	)
}

// Forward-path = Path
func forwardPath() parser.Fn {
	return path()
}

// <Postmaster>
func postmaster() parser.Fn {
	return parser.Seq(
		parser.Lit("<"),
		parser.Emit(
			parser.LitIgnoreCase("Postmaster"),
			argToken,
		),
		parser.Lit(">"),
	)
}

// Add anything that is not \r\n to the argument
func strArg() parser.Fn {
	return parser.Emit(
		parser.OneOrMore(parser.NotIn("\r\n")),
		argToken,
	)
}

// path <- '<' ( adl ':' )? mailbox '>'
func path() parser.Fn {
	return parser.Seq(
		parser.Lit("<"),
		parser.Optional(parser.Seq(adl(), parser.Lit(":"))),
		mailbox(),
		parser.Lit(">"),
	)
}

// adl <- atDomain ( ',' atDomain )*
func adl() parser.Fn {
	return parser.Seq(
		atDomain(),
		parser.ZeroOrMore(
			parser.Seq(parser.Lit(","), atDomain())),
	)
}

// atDomain <- '@' domain
func atDomain() parser.Fn {
	return parser.Seq(parser.Lit("@"), domain())
}

// mailbox <- localPart '@' ( domain / addrLiteral )
func mailbox() parser.Fn {
	return parser.Emit(
		parser.Seq(
			localPart(),
			parser.Lit("@"),
			parser.OneOf(domain(), addrLiteral()),
		),
		argToken,
	)
}

// Version of mailbox which separates localpart from domain
// mailbox <- localPart '@' ( domain / addrLiteral )
func mailboxParts() parser.Fn {
	return parser.Seq(
		parser.Emit(localPart(), localPartToken),
		parser.Lit("@"),
		parser.OneOf(
			parser.Emit(domain(), domainToken),
			parser.Emit(addrLiteral(), addrLiteralToken),
		),
	)
}

// localPart <- dotStr / quotedStr
func localPart() parser.Fn {
	return parser.OneOf(dotStr(), quotedStr())
}

// domain <- subdomain ( '.' subdomain )*
func domain() parser.Fn {
	return parser.Seq(
		subdomain(),
		parser.ZeroOrMore(
			parser.Seq(parser.Lit("."), subdomain())),
	)
}

// sub-domain = ( Let-dig [Ldh-str] ) / U-label
// U-label is currently a superset of the ascii subdomain
func subdomain() parser.Fn {
	return ulabel()
}

// Guessing from RFC-5892
func ulabel() parser.Fn {
	return parser.Seq(
		uLetDig(),
		parser.Optional(uLdhStr()),
	)
}

// dotStr <- atom ( '.' atom )*
func dotStr() parser.Fn {
	return parser.Seq(
		atom(),
		parser.ZeroOrMore(
			parser.Seq(parser.Lit("."), atom())),
	)
}

// atom <- atext+
func atom() parser.Fn {
	return parser.OneOrMore(atext())
}

// Unicode atext
func atext() parser.Fn {
	return parser.OneOf(
		alpha(),
		digit(),
		parser.NonASCII(),
		parser.IsIn("!#$%&'*+-/=?^ `{|}~"),
	)
}

// quotedStr <- '"' qcontentSMTP* '"'
func quotedStr() parser.Fn {
	quote := parser.Lit(`"`)
	return parser.Seq(quote, parser.ZeroOrMore(qcontentSMTP()), quote)
}

// qcontentSMTP <- qtextSMTP / qpairSMTP
func qcontentSMTP() parser.Fn {
	return parser.OneOf(qtextSMTP(), qpairSMTP())
}

// qtextSMTP = %d32-33 / %d35-91 / %d93-126 / UTF8-non-ascii
// Simplifies to: qtextSMTP =  [^"\]
func qtextSMTP() parser.Fn {
	return parser.NotIn(`^"\`)
}

// qpairSMTP <- '\' .
func qpairSMTP() parser.Fn {
	return parser.Seq(parser.Lit(`\`), parser.AnyRune())
}

// letDig <- alpha / digit
func letDig() parser.Fn {
	return parser.OneOf(alpha(), digit())
}

// Unicode version of letDig
func uLetDig() parser.Fn {
	return parser.OneOf(parser.IsLetter(), parser.IsDigit())
}

// Ldh-str = *( ALPHA / DIGIT / "-" ) Let-dig
func ldhStr() parser.Fn {
	return parser.ZeroOrMoreLazy(
		parser.OneOf(alpha(), digit(), parser.Lit("-")),
		letDig(),
	)
}

// Unicode version of ldhStr
func uLdhStr() parser.Fn {
	return parser.ZeroOrMoreLazy(
		parser.OneOf(parser.IsLetter(), parser.IsDigit(), parser.Lit("-")),
		uLetDig(),
	)
}

// sp <- ' '*
func sp() parser.Fn {
	return parser.ZeroOrMore(parser.Lit(" "))
}

// Mail-parameters = esmtp-param *(SP esmtp-param)
func mailParams() parser.Fn {
	return parser.Seq(
		esmtpParam(),
		parser.ZeroOrMore(
			parser.Seq(sp(), esmtpParam())))
}

// Rcpt-parameters = esmtp-param *(SP esmtp-param)
func rcptParams() parser.Fn {
	return parser.Seq(
		esmtpParam(),
		parser.ZeroOrMore(
			parser.Seq(sp(), esmtpParam())))
}

// esmtp-param = esmtp-keyword ["=" esmtp-value] { c.Params[key] = value }
func esmtpParam() parser.Fn {
	return parser.Seq(
		esmtpKeyword(),
		parser.Optional(
			parser.Seq(parser.Lit("="), esmtpValue()),
		),
	)
}

// esmtp-keyword  = (ALPHA / DIGIT) *(ALPHA / DIGIT / "-") { c.key = capture }
func esmtpKeyword() parser.Fn {
	return parser.Emit(
		parser.Seq(
			parser.OneOf(alpha(), digit()),
			parser.ZeroOrMore(
				parser.OneOf(alpha(), digit(), parser.Lit("-")))),
		paramKeyToken,
	)
}

// esmtp-value = 1*(%d33-60 / %d62-126 / UTF8-non-ascii)
func esmtpValue() parser.Fn {
	return parser.Emit(
		parser.OneOrMore(
			parser.OneOf(
				parser.Range('!', '<'),
				parser.Range('>', '~'),
				parser.NonASCII(),
			),
		),
		paramValueToken,
	)
}

// address-literal =
//  "[" ( IPv4-address-literal / IPv6-address-literal / General-address-literal ) "]"
func addrLiteral() parser.Fn {
	return parser.Seq(
		parser.Lit("["),
		parser.OneOf(ip4AddrLiteral(), ip6AddrLiteral(), generalAddrLiteral()),
		parser.Lit("]"),
	)
}

// IPv4-address-literal = Snum 3("."  Snum)
func ip4AddrLiteral() parser.Fn {
	return parser.Seq(
		snum(),
		parser.Repeat(3, 3, parser.Seq(parser.Lit("."), snum())),
	)
}

// 1*3DIGIT
func snum() parser.Fn {
	return parser.Repeat(1, 3, digit())
}

// IPv6-address-literal = "IPv6:" IPv6-addr
func ip6AddrLiteral() parser.Fn {
	return parser.Seq(parser.Lit("IPv6:"), ipv6Addr())
}

// IPv6-addr = IPv6-full / IPv6-comp / IPv6v4-full / IPv6v4-comp
func ipv6Addr() parser.Fn {
	return parser.OneOf(ipv6Full(), ipv6Comp(), ipv6v4Full(), ipv6v4Comp())
}

// IPv6-full = IPv6-hex 7(":" IPv6-hex)
func ipv6Full() parser.Fn {
	return parser.Seq(
		ipv6Hex(),
		parser.Repeat(7, 7, parser.Seq(parser.Lit(":"), ipv6Hex())),
	)
}

// IPv6-comp = [IPv6-hex *5(":" IPv6-hex)] "::" [IPv6-hex *5(":" IPv6-hex)]
func ipv6Comp() parser.Fn {
	colonHex := parser.Seq(parser.Lit(":"), ipv6Hex())
	return parser.Seq(
		parser.Optional(
			parser.Seq(ipv6Hex(), parser.Repeat(0, 5, colonHex))),
		parser.Lit("::"),
		parser.Optional(
			parser.Seq(ipv6Hex(), parser.Repeat(0, 5, colonHex))),
	)
}

// IPv6v4-full = IPv6-hex 5(":" IPv6-hex) ":" IPv4-address-literal
func ipv6v4Full() parser.Fn {
	return parser.Seq(
		ipv6Hex(),
		parser.Repeat(5, 5, parser.Seq(parser.Lit(":"), ipv6Hex())),
		parser.Lit(":"),
		ip4AddrLiteral(),
	)
}

// IPv6v4-comp = [IPv6-hex *3(":" IPv6-hex)] "::" [IPv6-hex *3(":" IPv6-hex) ":"]
//    IPv4-address-literal
func ipv6v4Comp() parser.Fn {
	return parser.Seq(
		parser.Optional(
			parser.Seq(
				ipv6Hex(),
				parser.Repeat(0, 3, parser.Seq(parser.Lit(":"), ipv6Hex())))),
		parser.Lit("::"),
		parser.Optional(
			parser.Seq(
				ipv6Hex(),
				parser.Repeat(0, 3, parser.Seq(parser.Lit(":"), ipv6Hex())),
				parser.Lit(":"),
			)),
		ip4AddrLiteral(),
	)
}

// General-address-literal = Standardized-tag ":" 1*dcontent
func generalAddrLiteral() parser.Fn {
	return parser.Seq(
		standardTag(),
		parser.Lit(":"),
		parser.OneOrMore(dcontent()),
	)
}

// Standardized-tag  = Ldh-str
func standardTag() parser.Fn {
	return ldhStr()
}

//  dcontent = %d33-90 / ; Printable US-ASCII
//             %d94-126   ; excl. "[", "\", "]"
func dcontent() parser.Fn {
	return parser.OneOf(parser.Range('!', 'Z'), parser.Range('^', '~'))
}

// IPv6-hex = 1*4HEXDIG
func ipv6Hex() parser.Fn {
	return parser.Repeat(1, 4, hexDigit())
}

func alpha() parser.Fn {
	return parser.OneOf(parser.Range('a', 'z'), parser.Range('A', 'Z'))
}

func digit() parser.Fn {
	return parser.Range('0', '9')
}

func hexDigit() parser.Fn {
	return parser.OneOf(digit(), parser.Range('a', 'f'))
}

// Package mime reads the structure of a multipart mail message without
// reading the complete message into memory.
package mime

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime"
	"mime/quotedprintable"
	"net/mail"
	"net/textproto"
	"strings"
	"time"
)

// Message holds the structure of an email message.
type Message struct {
	Part
	Subject   string
	MessageID string
	Date      time.Time
	Body      string
}

// Part holds part of a multipart mime message.
type Part struct {
	Header       textproto.MIMEHeader
	ContentStart int
	ContentEnd   int
	Parts        []Part
}

type headerReader struct {
	rdr *indexReader
	buf []byte
}

type textPart string

type partResult struct {
	part       Part
	isLastPart bool
	text       textPart
}

type multipartResult struct {
	parts []Part
	text  textPart
}

type partReader struct {
	maxDepth    int
	maxParts    int
	boundary    string
	captureText bool
	ir          *indexReader
}

// ErrBadHeader is an error that indicates a problem with a MIME Header line.
type ErrBadHeader string

const (
	// DepthLimit is the maximum supported depth of multipart MIME tree
	DepthLimit = 256
)

var (
	// ErrDepthLimitExceeded is reported if the MIME message exceeds DepthLimit
	ErrDepthLimitExceeded = errors.New("maximum depth of multipart MIME tree exceeded")
	// ErrTooManyParts is reported when a message contains too many MIME parts
	ErrTooManyParts = errors.New("multipart MIME has too many parts")
)

func errBadHeader(offset int) ErrBadHeader {
	return ErrBadHeader(fmt.Sprint("Bad header at offset ", offset))
}

func (e ErrBadHeader) Error() string { return string(e) }

// ReadMessage reads the structure of a multipart MIME message into a Message struct.
func ReadMessage(r io.Reader) (*Message, error) {
	ir := newIndexReader(r)
	message := &Message{}
	rdr := partReader{
		maxDepth:    16,
		maxParts:    1024,
		boundary:    "",
		captureText: true,
		ir:          ir,
	}
	res, err := rdr.readPart()
	message.Part = res.part
	message.Subject = textHeader(message.Header, "Subject")
	message.MessageID = messageID(message.Header, "Message-Id")
	message.Date = dateHeader(message.Header, "Date")
	message.Body = decodeBody(res.text)
	return message, err
}

func (p *partReader) readPart() (*partResult, error) {
	p.maxParts--
	if p.maxParts == 0 {
		return nil, ErrTooManyParts
	}
	start := p.ir.offset
	hdr, err := readMIMEHeader(p.ir)
	if err != nil {
		return nil, err
	}
	ret := &partResult{
		part:       Part{Header: hdr, ContentStart: start},
		isLastPart: false,
	}
	contentType := hdr.Get("Content-Type")
	switch {
	case strings.HasPrefix(contentType, "multipart"):
		var mp *multipartResult
		mp, err = p.readMultipart(contentType)
		if err != nil {
			ret.part.ContentEnd = p.ir.offset
			return ret, err
		}
		ret.part.Parts = mp.parts
		if ret.text == "" {
			ret.text = mp.text
		}
		ret.isLastPart, err = p.skipToBoundary()
	case p.captureText &&
		(contentType == "" || strings.HasPrefix(contentType, "text/plain")):
		var body []byte
		body, ret.isLastPart, err = p.readUntilBoundary()
		ret.text.set(textPart(body))
		p.captureText = false
	default:
		ret.isLastPart, err = p.skipToBoundary()
	}
	// The boundary line is not content
	ret.part.ContentEnd = p.ir.lastOffset
	return ret, err
}

func (p *partReader) readMultipart(contentType string) (*multipartResult, error) {
	_, params, err := mime.ParseMediaType(contentType)
	if err != nil {
		return nil, err
	}
	rdr := *p
	rdr.maxDepth--
	if rdr.maxDepth == 0 {
		return nil, ErrDepthLimitExceeded
	}
	rdr.boundary = "--" + params["boundary"]
	return rdr.readParts()
}

func (p *partReader) readParts() (*multipartResult, error) {
	ret := &multipartResult{
		parts: make([]Part, 0, 4),
	}
	_, err := p.skipToBoundary()
	if err != nil {
		return ret, err
	}
	for {
		pr, err := p.readPart()
		if pr != nil {
			ret.parts = append(ret.parts, pr.part)
			ret.text.set(pr.text)
		}
		if err != nil || pr.isLastPart {
			return ret, err
		}
	}
}

func (p *partReader) skipToBoundary() (bool, error) {
	if p.boundary == "" {
		err := p.ir.skipAll()
		return true, err
	}
	return p.ir.skipUntil(p.boundary)
}

func (p *partReader) readUntilBoundary() ([]byte, bool, error) {
	if p.boundary == "" {
		b, err := p.ir.readAll()
		return b, true, err
	}
	return p.ir.readUntil(p.boundary)
}

func readMIMEHeader(r *indexReader) (textproto.MIMEHeader, error) {
	hdr := make(textproto.MIMEHeader, 4)
	rdr := &headerReader{r, nil}
	for {
		offset := r.offset
		kv, err := rdr.headerLine()
		if err != nil || bytes.HasPrefix(kv, []byte("\r\n")) {
			break
		}
		i := bytes.IndexByte(kv, ':')
		if i < 0 {
			return hdr, errBadHeader(offset)
		}
		key := textproto.CanonicalMIMEHeaderKey(string(kv[:i]))
		if key == "" {
			return hdr, errBadHeader(offset)
		}
		value := kv[(i + 1):]
		value = bytes.Trim(value, " \t\r\n")
		copied := make([]byte, len(value))
		copy(copied, value)
		vlist := hdr[key]
		if vlist == nil {
			vlist = make([]string, 0, 1)
		}
		vlist = append(vlist, string(copied))
		hdr[key] = vlist
	}
	return hdr, nil
}

func (r *headerReader) headerLine() ([]byte, error) {
	line, err := r.rdr.readLineSlice()
	if err != nil {
		return line, err
	}
	if !r.rdr.possibleContinuation() {
		return line, err
	}
	r.buf = append(r.buf[:0], line...)
	for r.rdr.isContinuation() {
		line, err = r.rdr.readLineSlice()
		if err != nil {
			return r.buf, err
		}
		// Append the line but remove the trailing \r\n from r.buf
		r.buf = append(r.buf[:len(r.buf)-2], line...)
	}
	return r.buf, nil
}

// Sets the content of a text part if one has not been set already
func (t *textPart) set(s textPart) {
	if s == "" || *t != "" {
		return
	}
	*t = s
}

func textHeader(hdr textproto.MIMEHeader, fieldName string) string {
	value := hdr.Get(fieldName)
	if value == "" {
		return ""
	}
	dec := new(mime.WordDecoder)
	decoded, err := dec.DecodeHeader(value)
	if err != nil {
		return value
	}
	return decoded
}

func messageID(hdr textproto.MIMEHeader, fieldName string) string {
	value := hdr.Get(fieldName)
	if value == "" {
		return ""
	}
	parser := &mail.AddressParser{}
	addr, err := parser.Parse(value)
	if err != nil {
		return value
	}
	return addr.Address
}

func dateHeader(hdr textproto.MIMEHeader, fieldName string) time.Time {
	value := hdr.Get(fieldName)
	if value == "" {
		return time.Now()
	}
	d, err := mail.ParseDate(value)
	if err != nil {
		return time.Now()
	}
	return d
}

func decodeBody(body textPart) string {
	rdr := bytes.NewReader([]byte(body))
	dec := quotedprintable.NewReader(rdr)
	ret, err := ioutil.ReadAll(dec)
	if err != nil {
		return string(body)
	}
	return string(ret)
}

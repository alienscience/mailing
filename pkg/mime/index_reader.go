package mime

import (
	"bufio"
	"bytes"
	"io"
	"io/ioutil"
)

// Wraps a bufio.Reader keeping track of the amount of data returned
// to the caller.
type indexReader struct {
	br         *bufio.Reader
	offset     int
	lastOffset int
}

func newIndexReader(r io.Reader) *indexReader {
	return &indexReader{bufio.NewReader(r), 0, 0}
}

func (i *indexReader) readLineSlice() (line []byte, err error) {
	line, err = i.br.ReadSlice('\n')
	i.lastOffset = i.offset
	i.offset += len(line)
	return
}

// Skip data until a MIME multipart boundary.
// Returns (isClosingBoundary, error)
func (i *indexReader) skipUntil(boundary string) (bool, error) {
	normalB := []byte(boundary)
	closingB := []byte(boundary + "--")
	for {
		line, err := i.readLineSlice()
		if err != nil && err != io.EOF {
			return false, err
		}
		if bytes.HasPrefix(line, normalB) {
			return bytes.HasPrefix(line, closingB), nil
		}
		// Break on EOF
		if err != nil {
			return true, nil
		}
	}
}

// Skip until EOF
func (i *indexReader) skipAll() error {
	_, err := io.Copy(ioutil.Discard, i.br)
	return err
}

// Read data until a MIME multipart boundary.
func (i *indexReader) readUntil(boundary string) ([]byte, bool, error) {
	normalB := []byte(boundary)
	closingB := []byte(boundary + "--")
	var buf bytes.Buffer
	for {
		line, err := i.readLineSlice()
		if err != nil && err != io.EOF {
			return buf.Bytes(), false, err
		}
		if bytes.HasPrefix(line, normalB) {
			isClosing := bytes.HasPrefix(line, closingB)
			return buf.Bytes(), isClosing, err
		}
		_, _ = buf.Write(line)
	}
}

func (i *indexReader) readAll() ([]byte, error) {
	var buf bytes.Buffer
	for {
		line, err := i.readLineSlice()
		_, _ = buf.Write(line)
		if err == io.EOF {
			return buf.Bytes(), nil
		}
		if err != nil {
			return buf.Bytes(), err
		}
	}
}

func (i *indexReader) possibleContinuation() bool {
	if i.br.Buffered() > 0 {
		return i.isContinuation()
	}
	return true
}

func (i *indexReader) isContinuation() bool {
	peek, err := i.br.Peek(1)
	if err != nil || len(peek) == 0 {
		return false
	}
	return (peek[0] == ' ' || peek[0] == '\t')
}

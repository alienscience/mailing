package mime

import (
	"bufio"
	"io"
	"net/textproto"
	"path/filepath"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/r3labs/diff"
	"gitlab.com/alienscience/mailing/internal/testutil"
)

type hdrTest struct {
	name    string
	in      string
	isError bool
}

var hdrTests = []hdrTest{
	{name: "Swaks", in: headerText(
		"Date: Fri, 04 Oct 2019 17:38:32 +0200",
		"To: saul@localhost",
		"From: saul@fish.localdomain",
		"Subject: test Fri, 04 Oct 2019 17:38:32 +0200",
		"Message-Id: <20191004173832.005460@fish.localdomain>",
		"X-Mailer: swaks v20181104.0 jetmore.org/john/code/swaks/",
	)},
	{name: "Repeated", in: headerText(
		"X-Repeated: One",
		"X-Repeated: Two",
		"X-Repeated: Three",
	)},
	{name: "No colon", in: headerText(
		"To: fish@sea.com",
		"No colon here",
	), isError: true},
	{name: "Continuation", in: headerText(
		"Subject: A long subject with a continuation",
		" line following that just goes on and on",
		" without end",
		"From: fish@sea.com",
	)},
}

func TestHeader(t *testing.T) {
	for _, testCase := range hdrTests {
		tc := testCase // For scopelint
		t.Run(tc.name, func(t *testing.T) {
			header := tc.in
			rdr := newIndexReader(strings.NewReader(header))
			hdr, err := readMIMEHeader(rdr)
			if tc.isError {
				if err == nil {
					t.Error("Expected error:\n", tc.in)
				}
				return
			}
			if err != nil {
				t.Error(err)
				return
			}
			stdRdr := textproto.NewReader(bufio.NewReader(strings.NewReader(header)))
			mimeHeader, err := stdRdr.ReadMIMEHeader()
			if err != nil {
				t.Errorf("Error reading header with textproto: %v", err)
			}
			if !cmp.Equal(mimeHeader, hdr) {
				t.Error(mimeHeader, " != ", hdr)
			}
		})
	}
}

var messageTests = []string{"swaks", "multipart_alternative", "multipart_mixed", "windows_jpg"}

func TestMessages(t *testing.T) {
	for _, testCase := range messageTests {
		tc := testCase
		t.Run(tc, func(t *testing.T) {
			msgFile := tc + ".msg"
			expectedFile := filepath.Join("testdata", tc) + ".json"
			r := msgReader(t, msgFile)
			msg, err := ReadMessage(r)
			if err != nil {
				t.Error(err)
			}
			expected := Message{}
			testutil.FromJSONFile(t, expectedFile, &expected)
			d, err := diff.Diff(&expected, msg)
			if err != nil {
				t.Fatal(err)
			}
			if d != nil {
				t.Errorf("%#v", d)
				// testutil.ToJSONFile(t, tc+"-tmp.json", msg)
			}
		})
	}
}

func headerText(lines ...string) string {
	var ret strings.Builder
	for _, line := range lines {
		ret.WriteString(line)
		ret.WriteString("\r\n")
	}
	ret.WriteString("\r\n")
	return ret.String()
}

// Returns a Reader to a test file and ensures each line
// ends with \r\n
func msgReader(t *testing.T, filename string) io.Reader {
	path := filepath.Join("testdata", filename)
	return testutil.CrReader(t, path)
}

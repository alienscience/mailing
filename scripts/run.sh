#!/bin/sh

[ -f "$PWD/go.mod" ] || { cat <<EOF ; exit 1 ; }
$0 should be run from top level project directory.
EOF

cd ./cmd/mailing
go build
cd ../..
cmd/mailing/mailing -config configs/mailing.toml

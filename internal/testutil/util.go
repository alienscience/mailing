// Package testutil provides utilities for testing
// #nosec Only used for tests
package testutil

import (
	"bufio"
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
	"testing"
)

// CrReader returns a Reader to a test file and ensures each line
// ends with \r\n
func CrReader(t *testing.T, path string) io.Reader {
	file, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	rdr := bufio.NewReader(file)
	var buf bytes.Buffer
	for {
		line, err := rdr.ReadSlice('\n')
		if err == io.EOF {
			buf.Write(line)
			break
		}
		if err != nil {
			t.Fatal(err)
		}
		if bytes.HasSuffix(line, []byte("\r\n")) {
			buf.Write(line)
		} else {
			buf.Write(line[:len(line)-1])
			buf.WriteString("\r\n")
		}
	}
	return &buf
}

// CrFile returns a Buffer with each line ending with \r\n
func CrFile(t *testing.T, path string) []byte {
	rdr := CrReader(t, path)
	buf, err := ioutil.ReadAll(rdr)
	if err != nil {
		t.Fatal(err)
	}
	return buf
}

// FromJSONFile reads the given json file into the given data structure
func FromJSONFile(t *testing.T, path string, data interface{}) {
	file, err := os.Open(path)
	if err != nil {
		t.Fatal(err)
	}
	dec := json.NewDecoder(file)
	err = dec.Decode(data)
	if err != nil {
		t.Fatal(err)
	}
}

// ToJSONFile write the given data structure into a json file
func ToJSONFile(t *testing.T, path string, data interface{}) {
	file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0644)
	if err != nil {
		t.Fatal(err)
	}
	defer func() { _ = file.Close() }()
	enc := json.NewEncoder(file)
	enc.SetIndent("", "  ")
	err = enc.Encode(data)
	if err != nil {
		t.Fatal(err)
	}
}

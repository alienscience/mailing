// Package store stores mail messages.
package store

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"time"

	"gitlab.com/alienscience/mailing/pkg/mime"
	"gopkg.in/yaml.v3"
)

const timeAsFilename = "2006-01-02T15:04:05"

// A MailStore stores mail messages in a directory.
type MailStore struct {
	dir string
	seq int
}

// A MessageStore stores a single mail message and associated metadata
type MessageStore struct {
	recipients []string
	path       string
}

type multiwriter struct {
	io.Writer
	closers []io.WriteCloser
}

// NewMailstore creates a new mailstore.
func NewMailstore(dir string) (*MailStore, error) {
	if err := os.MkdirAll(dir, 0700); err != nil {
		return nil, err
	}
	return &MailStore{dir, 0}, nil
}

// MessageStore returns a MessageStore that will store a single message
func (m *MailStore) MessageStore(recipients []string) MessageStore {
	m.seq++
	now := time.Now().Format(timeAsFilename)
	id := fmt.Sprintf("%s-%06d", now, m.seq)
	pth := path.Join(m.dir, id)
	return MessageStore{
		recipients: recipients,
		path:       pth,
	}
}

// Writer returns a WriteCloser that will write an email message to the mailstore.
func (m *MessageStore) Writer() (io.WriteCloser, error) {
	pth := m.path + ".msg"
	file, err := os.OpenFile(pth, os.O_RDWR|os.O_CREATE, 0600) // #nosec
	if err != nil {
		return nil, err
	}
	rdr, pipew := io.Pipe()
	wr := multiWriteCloser(file, pipew)
	go m.parseMessage(rdr)
	return wr, nil
}

func (m MessageStore) parseMessage(rdr *io.PipeReader) {
	msg, err := mime.ReadMessage(rdr)
	if err != nil {
		_ = rdr.CloseWithError(err)
		return
	}
	err = m.saveParsedMessage(msg)
	if err != nil {
		_ = rdr.CloseWithError(err)
		return
	}
	// Ignore trailing data and close the reader
	_, _ = ioutil.ReadAll(rdr)
	_ = rdr.Close()
}

func (m MessageStore) saveParsedMessage(msg *mime.Message) error {
	pth := m.path + ".yml"
	file, err := os.OpenFile(pth, os.O_RDWR|os.O_CREATE, 0600) // #nosec
	if err != nil {
		return err
	}
	defer func() { _ = file.Close() }()
	enc := yaml.NewEncoder(file)
	defer func() { _ = enc.Close() }()
	err = enc.Encode(msg)
	return err
}

// AttachTrace attaches an SMTP trace to a message
func (m MessageStore) AttachTrace(trace []byte) error {
	pth := m.path + ".log"
	file, err := os.OpenFile(pth, os.O_RDWR|os.O_CREATE, 0600) // #nosec
	if err != nil {
		return err
	}
	defer func() { _ = file.Close() }()
	_, err = file.Write(trace)
	if err != nil {
		return err
	}
	return nil
}

func multiWriteCloser(wclosers ...io.WriteCloser) io.WriteCloser {
	writers := make([]io.Writer, len(wclosers))
	for i, w := range wclosers {
		writers[i] = w.(io.Writer)
	}
	return &multiwriter{Writer: io.MultiWriter(writers...), closers: wclosers}
}

func (w *multiwriter) Close() error {
	var ret error
	for _, c := range w.closers {
		err := c.Close()
		if ret == nil && err != nil {
			ret = err
		}
	}
	return ret
}
